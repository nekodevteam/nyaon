# Nyaon (にゃおん)
音関係のレポジトリです

## ライブラリ
### misc/midifile
- 簡易な midifile パーサです

### misc/vsthost
- VSTi のホストです
- VST SDK は vstsdk365_28_08_2015_build_66.zip を使っています

## アプリ
### midi2vsti
- [Sound Canvas VA](https://www.roland.com/jp/products/rc_sound_canvas_va/) を利用して MIDI ファイルを .wav にデコードします
