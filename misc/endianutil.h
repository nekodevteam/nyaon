﻿/**
 * @file	endianutil.h
 * @brief	エンディアン クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#pragma pack(push, 1)

/**
 * @brief 16bit ビッグエンディアン
 */
struct uint16be_t
{
	uint8_t b[2];		/*!< 値 */

#if defined(__cplusplus)
	/**
	 * 16ビット値取得
	 * @return 値
	 */
	operator uint16_t() const
	{
		return static_cast<uint16_t>((b[0] << 8) + b[1]);
	}

	/**
	 * 16ビット値設定
	 * @param[in] value 値
	 * @return 自分
	 */
	const uint16be_t& operator=(uint16_t value)
	{
		b[0] = static_cast<uint8_t>(value >> 8);
		b[1] = static_cast<uint8_t>(value);
		return *this;
	}
#endif	/* defined(__cplusplus) */
};

/**
 * @brief 32bit ビッグエンディアン
 */
struct uint32be_t
{
	uint8_t b[4];		/*!< 値 */

#if defined(__cplusplus)
	/**
	 * 32ビット値取得
	 * @return 値
	 */
	operator uint32_t() const
	{
		return static_cast<uint32_t>((b[0] << 24) + (b[1] << 16) + (b[2] << 8) + b[3]);
	}

	/**
	 * 32ビット値設定
	 * @param[in] value 値
	 * @return 自分
	 */
	const uint32be_t& operator=(uint32_t value)
	{
		b[0] = static_cast<uint8_t>(value >> 24);
		b[1] = static_cast<uint8_t>(value >> 16);
		b[2] = static_cast<uint8_t>(value >> 8);
		b[3] = static_cast<uint8_t>(value);
		return *this;
	}
#endif	/* defined(__cplusplus) */
};

#pragma pack(pop)
