﻿/**
 * @file	binary.h
 * @brief	バイナリ操作クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <memory.h>
#include <vector>

/**
 * @brief バイナリ ヘルパ テンプレート
 */
template<class T> class CBinaryTmpl : public std::vector<T>
{
public:
	/** 基底クラス タイプ */
	typedef std::vector<T> binary;

	/**
	 * コンストラクタ
	 */
	CBinaryTmpl<T>()
	{
	}

	/**
	 * コンストラクタ
	 * @param[in] bin コピー元
	 */
	CBinaryTmpl<T>(const binary& bin)
	{
		*(static_cast<binary*>(this)) = bin;
	}

	/**
	 * コンストラクタ
	 * @param[in] nSize サイズ
	 * @param[in] lpData データ
	 */
	CBinaryTmpl<T>(size_t nSize, const void* lpData = NULL)
	{
		binary::resize(nSize);
		if ((lpData) && (!binary::empty()))
		{
			memcpy(&binary::at(0), lpData, binary::size() * sizeof(T));
		}
	}

	/**
	 * コピー オペレータ
	 * @param[in] bin コピー元
	 * @return インスタンス
	 */
	CBinaryTmpl<T>& operator=(const binary& bin)
	{
		*(static_cast<binary*>(this)) = bin;
		return *this;
	}

	/**
	 * + オペレータ
	 * @param[in] bin 追加データ
	 * @return コピー
	 */
	CBinaryTmpl<T> operator+(const binary& bin)
	{
		CBinaryTmpl<T> tmp(*this);
		tmp.insert(tmp.end(), bin.begin(), bin.end());
		return tmp;
	}

	/**
	 * += オペレータ
	 * @param[in] bin 追加データ
	 * @return インスタンス
	 */
	CBinaryTmpl<T>& operator+=(const binary& bin)
	{
		binary::insert(binary::end(), bin.begin(), bin.end());
		return *this;
	}

	/**
	 * ファイルを読む
	 * @param[in] fp ファイル
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Read(FILE* fp)
	{
		fseek(fp, 0, SEEK_END);
		const size_t nSize = ftell(fp) / sizeof(T);
		fseek(fp, 0, SEEK_SET);

		binary::resize(nSize);
		if (!binary::empty())
		{
			const size_t r = fread(&binary::at(0), sizeof(T), binary::size(), fp);
			binary::resize(r);
		}
		return true;
	}

	/**
	 * ファイルを読む
	 * @param[in] lpFilename ファイル名
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Read(const char* lpFilename)
	{
		bool ret = false;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (fopen_s(&fp, lpFilename, "rb") == 0)
#else	// (_MSC_VER >= 1500)
		FILE* fp = fopen(lpFilename, "rb");
		if (fp)
#endif	// (_MSC_VER >= 1500)
		{
			ret = Read(fp);
			fclose(fp);
		}
		return ret;
	}

	/**
	 * ファイルと比較する
	 * @param[in] lpFilename ファイル名
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Compare(const char* lpFilename) const
	{
		CBinaryTmpl<T> tmp(*this);
		return (tmp.Read(lpFilename)) && (*this == tmp);
	}

	/**
	 * ファイルに保存する
	 * @param[in] fp ファイル
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Write(FILE* fp) const
	{
		if (!binary::empty())
		{
			fwrite(&binary::at(0), binary::size(), sizeof(T), fp);
		}
		return true;
	}

	/**
	 * ファイルに保存する
	 * @param[in] lpFilename ファイル名
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Write(const char* lpFilename) const
	{
		bool ret = false;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (fopen_s(&fp, lpFilename, "wb") == 0)
#else	// (_MSC_VER >= 1500)
		FILE* fp = fopen(lpFilename, "wb");
		if (fp)
#endif	// (_MSC_VER >= 1500)
		{
			ret = Write(fp);
			fclose(fp);
		}
		return ret;
	}

#ifdef _WIN32
	/**
	 * ファイルを読む
	 * @param[in] lpFilename ファイル名
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Read(const wchar_t* lpFilename)
	{
		bool ret = false;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (_wfopen_s(&fp, lpFilename, L"rb") == 0)
#else	// (_MSC_VER >= 1500)
		FILE* fp = _wfopen(lpFilename, L"rb");
		if (fp)
#endif	// (_MSC_VER >= 1500)
		{
			ret = Read(fp);
			fclose(fp);
		}
		return ret;
	}

	/**
	 * ファイルと比較する
	 * @param[in] lpFilename ファイル名
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Compare(const wchar_t* lpFilename) const
	{
		CBinaryTmpl<T> tmp(*this);
		return (tmp.Read(lpFilename)) && (*this == tmp);
	}

	/**
	 * ファイルに保存する
	 * @param[in] lpFilename ファイル名
	 * @retval true 成功
	 * @retval false 失敗
	 */
	bool Write(const wchar_t* lpFilename) const
	{
		bool ret = false;
#if (_MSC_VER >= 1500 /*Visual Studio 2008*/)
		FILE* fp;
		if (_wfopen_s(&fp, lpFilename, L"wb") == 0)
#else	// (_MSC_VER >= 1500)
		FILE* fp = _wfopen(lpFilename, L"wb");
		if (fp)
#endif	// (_MSC_VER >= 1500)
		{
			ret = Write(fp);
			fclose(fp);
		}
		return ret;
	}
#endif	// _WIN32

	/**
	 * エンディアン スワップ
	 */
	void SwapEndian()
	{
		if (!binary::empty())
		{
			size_t i = 0;
			size_t n = binary::size() - 1;
			while (i < n)
			{
				std::swap(binary::at(i), binary::at(n));
				i++;
				n--;
			}
		}
	}
};

typedef CBinaryTmpl<uint8_t>	CBinary;		/*!< バイナリ型 */
