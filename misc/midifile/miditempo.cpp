﻿/**
 * @file	miditempo.cpp
 * @brief	MIDI テンポ クラスの動作の定義を行います
 */

#include "stdafx.h"
#include "miditempo.h"
#include <memory.h>
#include <limits.h>
#include <stdlib.h>

#define FRACTION_BITS		16									/*!< 小数点以下ビット数 */
#define FRACTION_MASK		((1 << FRACTION_BITS) - 1)			/*!< 小数点以下ビット マスク */

/**
 * コンストラクタ
 * @param[in] nBase ベース
 */
CMidiTempo::CMidiTempo(unsigned int nBase)
	: m_nBase(nBase)
	, m_nScaleFactor(0)
	, m_nTimeBase(0)
	, m_nTempo(MIDIIF_DEFAULT_TEMPO)
{
}

/**
 * タイムベース設定
 * @param[in] nTimeBase タイムベース
 */
void CMidiTempo::SetTimeBase(unsigned int nTimeBase)
{
	if (m_nTimeBase != nTimeBase)
	{
		m_nTimeBase = nTimeBase;
		Update();
	}
}

/**
 * テンポ設定
 * @param[in] nTempo テンポ
 */
void CMidiTempo::SetTempo(unsigned int nTempo)
{
	if (m_nTempo != nTempo)
	{
		m_nTempo = nTempo;
		Update();
	}
}

/**
 * Tick からカウント値を得る
 * @param[in] nTick TICK
 * @return カウント値
 */
unsigned int CMidiTempo::GetCount(unsigned int nTick) const
{
	return (nTick * m_nScaleFactor) >> FRACTION_BITS;
}

/**
 * Tick からカウント値を得る
 * @param[in] nTick TICK
 * @param[in,out] nFraction カウンタ端数
 * @return カウント値
 */
unsigned int CMidiTempo::GetCount(unsigned int nTick, unsigned int& nFraction) const
{
	const unsigned int nWaitCount = (nTick * m_nScaleFactor) + nFraction;
	nFraction = nWaitCount & FRACTION_MASK;
	return (nWaitCount >> FRACTION_BITS);
}

/**
 * Tick からカウント値を得る(64bit 版)
 * @param[in] nTick TICK
 * @param[in,out] nFraction カウンタ端数
 * @return カウント値
 */
unsigned int CMidiTempo::GetCount64(unsigned int nTick, unsigned int& nFraction) const
{
	const uint64_t nWaitCount = (static_cast<uint64_t>(nTick) * m_nScaleFactor) + nFraction;
	nFraction = static_cast<unsigned int>(nWaitCount) & FRACTION_MASK;
	return static_cast<unsigned int>(nWaitCount >> FRACTION_BITS);
}

/**
 * Tick からカウント値を得る(signed 版)
 * @param[in] nTick TICK
 * @param[in,out] nFraction カウンタ端数
 * @return カウント値
 */
int CMidiTempo::GetSignedCount(int nTick, unsigned int& nFraction) const
{
	const int nWaitCount = (nTick * static_cast<int>(m_nScaleFactor)) + nFraction;
	nFraction = nWaitCount & FRACTION_MASK;
	return (nWaitCount >> FRACTION_BITS);
}

/**
 * Tick からカウント値を得る(signed 64bit版)
 * @param[in] nTick TICK
 * @param[in,out] nFraction カウンタ端数
 * @return カウント値
 */
int CMidiTempo::GetSignedCount64(int nTick, unsigned int& nFraction) const
{
	const int64_t nWaitCount = (static_cast<int64_t>(nTick) * static_cast<int>(m_nScaleFactor)) + nFraction;
	nFraction = static_cast<unsigned int>(nWaitCount) & FRACTION_MASK;
	return static_cast<int>(nWaitCount >> FRACTION_BITS);
}

/**
 * カウント値から TICK を得る
 * @param[in] nCount カウント
 * @param[in] nFraction カウンタ端数
 * @return TICK
 */
unsigned int CMidiTempo::LookupTick(unsigned int nCount, unsigned int nFraction) const
{
	if (nCount == 0)
	{
		return 0;
	}

	nCount = (nCount << FRACTION_BITS) - nFraction;
	if ((nCount < m_nScaleFactor) || (m_nScaleFactor == 0))
	{
		return 0;
	}
	return nCount / m_nScaleFactor;
}

/**
 * カウント値から TICK を得る(signed 版)
 * @param[in] nCount カウント
 * @param[in] nFraction カウンタ端数
 * @return TICK
 */
int CMidiTempo::LookupSignedTick(int nCount, unsigned int nFraction) const
{
	return ((nCount << FRACTION_BITS) - nFraction) / m_nScaleFactor;
}

/**
 * カウント値から TICK を得る
 * @param[in] nCount カウント
 * @param[in] nFraction カウンタ端数
 * @return TICK
 */
int CMidiTempo::LookupSignedTick64(int nCount, unsigned int nFraction) const
{
	const int64_t n = static_cast<int64_t>(nCount);
	return static_cast<int>(((n << FRACTION_BITS) - nFraction) / m_nScaleFactor);
}

/**
 * 更新
 */
void CMidiTempo::Update()
{
	if ((m_nBase == 0) || (m_nTimeBase == 0))
	{
		return;
	}

	const double fScale = static_cast<double>(m_nTempo) / 1000000.0 * m_nBase / m_nTimeBase;
	m_nScaleFactor = static_cast<unsigned int>((fScale * (1 << FRACTION_BITS)) + 0.5);
}
