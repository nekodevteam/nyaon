﻿/**
 * @file	miditick.cpp
 * @brief	MIDI TICK クラスの動作の定義を行います
 */

#include "stdafx.h"
#include "miditick.h"
#include <algorithm>
#include <memory.h>
#include <limits.h>
#include <stdlib.h>

/**
 * コンストラクタ
 * @param[in] nBase ベース
 */
CMidiTick::CMidiTick(unsigned int nBase)
	: m_tempo(nBase)
	, m_nWaitFraction(0)
	, m_nRemainCount(0)
{
}

/**
 * デストラクタ
 */
CMidiTick::~CMidiTick()
{
}

/**
 * 頭出し
 * @return 有効チャネル数
 */
unsigned int CMidiTick::Rewind()
{
	m_tempo.SetTimeBase(GetTimeBase());
	m_tempo.SetTempo(MIDIIF_DEFAULT_TEMPO);

	m_nWaitFraction = 0;
	m_nRemainCount = 0;

	return CMidiFile::Rewind();
}

/**
 * メッセージ
 * @param[in] track トラック ポインタ
 * @param[in] nMsg メッセージ タイプ
 * @param[in] pData メッセージ データ
 * @param[in] nSize メッセージ サイズ
 * @retval true 成功
 * @retval false 失敗
 */
bool CMidiTick::OnMessage(CMidiTrack* track, int nMsg, const unsigned char* pData, unsigned int nSize)
{
	if (nMsg == 0x51)
	{
		if (nSize >= 3)
		{
			const unsigned int nTempo = (pData[0] << 16) | (pData[1] << 8) | pData[2];
			m_tempo.SetTempo(nTempo);
		}
	}
	return CMidiFile::OnMessage(track, nMsg, pData, nSize);
}

/**
 * 実行
 * @param[in] nCount 処理する最大カウント
 * @return 経過カウント
 */
unsigned int CMidiTick::Step(unsigned int nCount)
{
	if (m_nEnableTracks == 0)
	{
		return 0;
	}

	nCount += m_nRemainCount;
	m_nRemainCount = 0;

	unsigned int nElapsed = 0;
	while (1)
	{
		unsigned int nTick = m_tempo.LookupTick(nCount, m_nWaitFraction);
		if (nTick == 0)
		{
			break;
		}

		const unsigned int nRemainTick = GetNextTick();
		nTick = (std::min)(nTick, nRemainTick);

		const unsigned int nElapsedCount = m_tempo.GetCount(nTick, m_nWaitFraction);
		nElapsed += nElapsedCount;
		nCount -= nElapsedCount;

		if (ExecuteStep(nTick) == 0)
		{
			break;
		}
	}

	m_nRemainCount = nCount;
	return nElapsed;
}

/**
 * TICK を得る
 * @param[in] nDelay 
 * @return TICK
 */
unsigned int CMidiTick::GetDelayTick(int nDelay) const
{
	unsigned int nTick = GetTick();

	if (nDelay > 0)
	{
		const unsigned int nDelayCount = (nDelay * m_tempo.GetBase()) / 1000;
		unsigned int nDelayTick = m_tempo.LookupTick(nDelayCount);
		nDelayTick = (std::min)(nDelayTick, nTick);
		nTick -= nDelayTick;
	}

	return nTick;
}
