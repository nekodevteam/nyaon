﻿/**
 * @file	miditick.h
 * @brief	MIDI TICK クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "midifile.h"
#include "miditempo.h"

/**
 * @brief MIDI TICK クラス
 */
class CMidiTick : public CMidiFile
{
public:
	CMidiTick(unsigned int nBase);
	virtual ~CMidiTick();
	virtual unsigned int Rewind();
	unsigned int Step(unsigned int nCount);
	unsigned int GetDelayTick(int nDelay) const;

protected:
	virtual bool OnMessage(CMidiTrack* track, int nMsg, const unsigned char* pData, unsigned int nSize);

private:
	CMidiTempo m_tempo;						/*!< テンポ */
	unsigned int m_nWaitFraction;			/*!< 待ちカウンタ */
	unsigned int m_nRemainCount;			/*!< 残りカウント */
};
