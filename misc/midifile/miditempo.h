﻿/**
 * @file	miditempo.h
 * @brief	MIDI テンポ クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#define MIDIIF_DEFAULT_TEMPO		500000		/*!< デフォルト */

/**
 * @brief MIDI テンポ クラス (Tick → Counter)
 */
class CMidiTempo
{
public:
	CMidiTempo(unsigned int nBase);
	unsigned int GetBase() const;
	void SetTimeBase(unsigned int nTimeBase);
	void SetTempo(unsigned int nTempo);
	unsigned int GetTempo() const;
	unsigned int GetCount(unsigned int nTick) const;
	unsigned int GetCount(unsigned int nTick, unsigned int& nFraction) const;
	unsigned int GetCount64(unsigned int nTick, unsigned int& nFraction) const;
	int GetSignedCount(int nTick, unsigned int& nFraction) const;
	int GetSignedCount64(int nTick, unsigned int& nFraction) const;
	unsigned int LookupTick(unsigned int nCount, unsigned int nFraction = 0) const;
	int LookupSignedTick(int nCount, unsigned int nFraction = 0) const;
	int LookupSignedTick64(int nCount, unsigned int nFraction = 0) const;

protected:
	unsigned int m_nBase;					/*!< ベース(カウンタ周期) */
	unsigned int m_nScaleFactor;			/*!< スケーラ― */
	unsigned int m_nTimeBase;				/*!< タイム ベース */
	unsigned int m_nTempo;					/*!< テンポ */

private:
	void Update();
};

/**
 * ベース値を得る
 * @return ベース値
 */
inline unsigned int CMidiTempo::GetBase() const
{
	return m_nBase;
}

/**
 * 今のテンポ値を得る
 * @return テンポ
 */
inline unsigned int CMidiTempo::GetTempo() const
{
	return m_nTempo;
}
