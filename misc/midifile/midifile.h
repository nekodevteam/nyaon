﻿/**
 * @file	midifile.h
 * @brief	MIDI ファイル クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

class CMidiFile;

/**
 * @brief MIDI トラック データ クラス
 */
class CMidiTrack
{
public:
	CMidiTrack();
	unsigned int SetData(const void* lpcvData, size_t nDataSize);
	unsigned int ExecuteStep(CMidiFile& mf, unsigned int nStepTime);
	void SetModule(int nModule);
	int GetModule() const;

private:
	const unsigned char* m_lpcsData;	/*!< 現在のデータ ポインタ */
	const unsigned char* m_lpcsDataEnd;	/*!< データ エンド ポインタ */
	unsigned int m_nStep;				/*!< 残りステップ タイム */
	unsigned char m_cStatus;			/*!< ランニング ステータス */
	int m_nModule;						/*!< モジュール番号 */
	size_t GetRemain() const;
	int GetByte();
	unsigned int LookupDelta(size_t& nSize) const;
	unsigned int GetDelta();
	const unsigned char* GetDeltaData(unsigned int& nSize);
	bool Exclusive(CMidiFile& mf, bool bBulk);
};

/**
 * モジュール ID を設定
 * @param[in] nModule モジュール ID
 */
inline void CMidiTrack::SetModule(int nModule)
{
	m_nModule = nModule;
}

/**
 * モジュール ID を取得
 * @return モジュール ID
 */
inline int CMidiTrack::GetModule() const
{
	return m_nModule;
}

/**
 * 残りデータ サイズを得る
 * @return 残りデータ サイズ
 */
inline size_t CMidiTrack::GetRemain() const
{
	return m_lpcsDataEnd - m_lpcsData;
}

/**
 * @brief MIDI ファイル パース クラス
 */
class CMidiFile
{
public:
	CMidiFile();
	virtual ~CMidiFile();
	void Clear();
	virtual bool SetData(const void* lpcvData, size_t nDataSize);
	virtual unsigned int Rewind();
	unsigned int ExecuteStep(unsigned int nStep);
	unsigned int GetTimeBase() const;
	unsigned int GetNextTick() const;
	unsigned int GetTick() const;
	bool IsPlaying() const;

protected:
	const unsigned char* m_lpcsData;		/*!< MIDI データ */
	size_t m_nDataSize;						/*!< MIDI データ サイズ */
	unsigned int m_nMaxTracks;				/*!< トラック数 */
	unsigned int m_nTimeBase;				/*!< タイム ベース */
	unsigned int m_nEnableTracks;			/*!< 有効トラック数 */
	unsigned int m_nStepTick;				/*!< ステップ数 */
	unsigned int m_nRemainTick;				/*!< ウェイト ステップ数残り */
	unsigned int m_nTotalTick;				/*!< TICK */

	CMidiTrack* m_track;					/*!< トラック */

	virtual bool OnMessage(CMidiTrack* track, int nMsg, const unsigned char* pData, unsigned int nSize);
	virtual void OnShortMsg(CMidiTrack* track, unsigned int nMsg);
	virtual void OnLongMsg(CMidiTrack* track, const void* pData, unsigned int nSize);

private:
	unsigned int ExecuteStepInner(unsigned int nStep);

	friend class CMidiTrack;
};

/**
 * タイム ベースを取得
 * @return タイム ベース
 */
inline unsigned int CMidiFile::GetTimeBase() const
{
	return m_nTimeBase;
}

/**
 * 次のイベントまでの TICK を得る
 * @return 残り TICK
 */
inline unsigned int CMidiFile::GetNextTick() const
{
	return m_nRemainTick;
}

/**
 * 現在の TICK を得る
 * @return TICK
 */
inline unsigned int CMidiFile::GetTick() const
{
	return m_nTotalTick + (m_nStepTick - m_nRemainTick);
}

/**
 * 再生中?
 * @retval true 再生中
 * @retval false 再生終了
 */
inline bool CMidiFile::IsPlaying() const
{
	return (m_nEnableTracks != 0);
}

/**
 * ショート メッセージ
 * @param[in] track トラック ポインタ
 * @param[in] nMsg メッセージ
 */
inline void CMidiFile::OnShortMsg(CMidiTrack* track, unsigned int nMsg)
{
}

/**
 * ロング メッセージ
 * @param[in] track トラック ポインタ
 * @param[in] pData メッセージ データ
 * @param[in] nSize メッセージ サイズ
 */
inline void CMidiFile::OnLongMsg(CMidiTrack* track, const void* pData, unsigned int nSize)
{
}
