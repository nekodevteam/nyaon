﻿/**
 * @file	midifile.cpp
 * @brief	MIDI ファイル クラスの動作の定義を行います
 */

#include "stdafx.h"
#include "midifile.h"
#include <algorithm>
#include <assert.h>
#include <memory.h>
#include <limits.h>
#include <stdlib.h>
#include "../endianutil.h"

/**
 * @brief MIDI ブロック
 */
struct MidiBlock
{
	char sig[4];		/*!< シグネチャ */
	uint32be_t size;	/*!< サイズ(BE) */
};

// ---- midifile

/**
 * コンストラクタ
 */
CMidiTrack::CMidiTrack()
	: m_lpcsData(NULL)
	, m_lpcsDataEnd(NULL)
	, m_nStep(0)
	, m_cStatus(0)
	, m_nModule(0)
{
}

/**
 * データ セット
 * @param[in] lpcvData データ ポインタ
 * @param[in] nDataSize データ サイズ
 * @return データ サイズ (0で失敗)
 */
unsigned int CMidiTrack::SetData(const void* lpcvData, size_t nDataSize)
{
	if ((lpcvData == NULL) || (nDataSize < sizeof(MidiBlock)))
	{
		return 0;
	}
	const MidiBlock* mb = static_cast<const MidiBlock*>(lpcvData);
	if (::memcmp(mb->sig, "MTrk", 4) != 0)
	{
		return 0;
	}
	const unsigned int nSize = static_cast<unsigned int>(mb->size);
	const unsigned int nTrackSize = sizeof(MidiBlock) + nSize;
	if (nDataSize < nTrackSize)
	{
		return 0;
	}

	m_lpcsData = reinterpret_cast<const unsigned char*>(mb + 1);
	m_lpcsDataEnd = m_lpcsData + nSize;
	m_nStep = GetDelta();
	m_cStatus = 0x80;
	return nTrackSize;
}

/**
 * バイト データを得る
 * @return データ (-1 でEOD)
 */
int CMidiTrack::GetByte()
{
	if (m_lpcsData >= m_lpcsDataEnd)
	{
		return -1;
	}
	const int c = *m_lpcsData++;
	return c;
}

/**
 * デルタ値を得る
 * @param[out] nSize データ サイズ
 * @return 値
 */
unsigned int CMidiTrack::LookupDelta(size_t& nSize) const
{
	unsigned int nDelta = 0;

	const unsigned char* p = m_lpcsData;
	while (p < m_lpcsDataEnd)
	{
		const unsigned char c = *p++;
		nDelta <<= 7;
		nDelta += (c & 0x7f);
		if ((c & 0x80) == 0)
		{
			break;
		}
	}
	nSize = p - m_lpcsData;
	return nDelta;
}

/**
 * デルタ値を得る
 * @return 値
 */
unsigned int CMidiTrack::GetDelta()
{
	size_t nSize;
	const unsigned int nDelta = LookupDelta(nSize);
	m_lpcsData += nSize;
	return nDelta;
}

/**
 * デルタ データを得る
 * @param[out] nSize デルタ データ サイズ
 * @return デルタ データ ポインタ
 */
const unsigned char* CMidiTrack::GetDeltaData(unsigned int& nSize)
{
	size_t nLength;
	const unsigned int nDelta = LookupDelta(nLength);

	const size_t nRemain = GetRemain() - nLength;
	if (nDelta <= nRemain)
	{
		const unsigned char* ret = m_lpcsData + nLength;
		m_lpcsData = ret + nDelta;
		nSize = nDelta;
		return ret;
	}
	else
	{
		m_lpcsData = m_lpcsDataEnd;
		nSize = 0;
		return NULL;
	}
}

/**
 * エクスクルーシヴ
 * @param[in] mf ファイル インスタンス
 * @param[in] bBulk バルク モード
 * @retval true 成功
 * @retval false 失敗
 */
bool CMidiTrack::Exclusive(CMidiFile& mf, bool bBulk)
{
	unsigned int nSize;
	const unsigned char* pData = GetDeltaData(nSize);
	if (pData == NULL)
	{
		return false;
	}

	if (bBulk)
	{
		mf.OnLongMsg(this, pData, nSize);
	}
	else
	{
		unsigned char sExcv[128];
		if (nSize < (sizeof(sExcv) - 1))
		{
			sExcv[0] = 0xf0;
			memcpy(sExcv + 1, pData, nSize);
			mf.OnLongMsg(this, sExcv, 1 + nSize);
		}
	}
	return true;
}

/**
 * ステップ
 * @param[in] mf ファイル インスタンス
 * @param[in] nStep ステップ数
 * @return 次のステップ数
 */
unsigned int CMidiTrack::ExecuteStep(CMidiFile& mf, unsigned int nStep)
{
	assert(m_nStep >= nStep);

	m_nStep -= nStep;
	while (!m_nStep)
	{
		const int c1 = GetByte();
		if (c1 < 0)
		{
			break;
		}
		else if (c1 < 0xf0)
		{
			unsigned int nMsg = c1;
			if (c1 & 0x80)
			{
				m_cStatus = c1;
				const int c2 = GetByte();
				if (c2 < 0)
				{
					return 0;
				}
				nMsg = nMsg | (c2 << 8);
			}
			else
			{
				nMsg = (nMsg << 8) | m_cStatus;
			}

			if ((c1 < 0xc0) || (c1 >= 0xe0))
			{
				const int c3 = GetByte();
				if (c3 < 0)
				{
					break;
				}
				nMsg = nMsg | (c3 << 16);
			}

			mf.OnShortMsg(this, nMsg);
		}
		else if ((c1 == 0xf0) || (c1 == 0xf7))
		{
			if (!Exclusive(mf, (c1 == 0xf7)))
			{
				return 0;
			}
		}
		else if ((c1 == 0xf1) || (c1 == 0xf3) || (c1 == 0xf5))
		{
			GetByte();
		}
		else if (c1 == 0xf2)
		{
			GetByte();
			GetByte();
		}
		else if (c1 == 0xff)
		{
			const int c2 = GetByte();
			if (c2 < 0)
			{
				return 0;
			}

			unsigned int nSize;
			const unsigned char* pData = GetDeltaData(nSize);
			if (pData == NULL)
			{
				return 0;
			}

			if (c2 == 0x2f)
			{
				return 0;
			}

			if (!mf.OnMessage(this, c2, pData, nSize))
			{
				return 0;
			}
		}

		m_nStep = GetDelta();
	}

	return m_nStep;
}

// ----

/**
 * @brief MIDI ヘッダー
 */
struct MidiHeader
{
	uint16be_t format;		/*!< フォーマット(BE) */
	uint16be_t track;		/*!< トラック数(BE) */
	uint16be_t timebase;	/*!< タイムベース(BE) */
};

/**
 * コンストラクタ
 */
CMidiFile::CMidiFile()
	: m_lpcsData(NULL)
	, m_nDataSize(0)
	, m_nMaxTracks(0)
	, m_nTimeBase(0)
	, m_nEnableTracks(0)
	, m_nStepTick(0)
	, m_nRemainTick(0)
	, m_nTotalTick(0)
	, m_track(NULL)
{
}

/**
 * デストラクタ
 */
CMidiFile::~CMidiFile()
{
	Clear();
}

/**
 * クリア
 */
void CMidiFile::Clear()
{
	if (m_track != NULL)
	{
		delete[] m_track;
		m_track = NULL;
	}
	m_nMaxTracks = 0;
	m_nEnableTracks = 0;
}

/**
 * データ セット
 * @param[in] lpcvData データ ポインタ
 * @param[in] nDataSize データ サイズ
 * @retval true 成功
 * @retval false 失敗
 */
bool CMidiFile::SetData(const void* lpcvData, size_t nDataSize)
{
	if (lpcvData == NULL)
	{
		return false;
	}

	const MidiBlock* mblk = static_cast<const MidiBlock*>(lpcvData);
	const MidiHeader* mhead = reinterpret_cast<const MidiHeader*>(mblk + 1);
	if (nDataSize < (sizeof(*mblk) + sizeof(*mhead)))
	{
		return false;
	}
	if (memcmp(mblk->sig, "MThd", 4) != 0)
	{
		// DEBUGLOG(TEXT("mid: not hit MThd"));
		return false;
	}
	const unsigned int headsize = static_cast<unsigned int>(mblk->size);
	if (headsize < sizeof(MidiHeader))
	{
		return false;
	}
	const unsigned int format = static_cast<unsigned int>(mhead->format);
	unsigned int tracks = static_cast<unsigned int>(mhead->track);
	const unsigned int timebase = static_cast<unsigned int>(mhead->timebase);
	// DEBUGLOG(TEXT("mid: format %d"), format);
	// DEBUGLOG(TEXT("mid: tracks %d"), tracks);
	// DEBUGLOG(TEXT("mid: timebase %d"), timebase);
	if (format == 0)
	{
		tracks = 1;
	}
	else if (format != 1)
	{
		tracks = 0;
	}
	if ((tracks == 0) || (timebase == 0))
	{
		return false;
	}

	if (nDataSize < (headsize + sizeof(MidiBlock)))
	{
		return false;
	}

	m_track = new CMidiTrack [tracks];

	m_lpcsData = static_cast<const unsigned char*>(lpcvData) + sizeof(*mblk) + headsize;
	m_nDataSize = nDataSize - sizeof(*mblk) - headsize;
	m_nMaxTracks = tracks;
	m_nTimeBase = timebase;
	if (Rewind() == 0)
	{
		// DEBUGLOG(TEXT("mid: none channel"));
		return false;
	}
	// DEBUGLOG(TEXT("mid: open success"));
	return true;
}

/**
 * 頭出し
 * @return 有効チャネル数
 */
unsigned int CMidiFile::Rewind()
{
	const unsigned char* lpcsData = m_lpcsData;
	size_t nDataSize = m_nDataSize;

	unsigned int nEnableTracks = 0;
	do
	{
		const unsigned int nTrackSize = m_track[nEnableTracks].SetData(lpcsData, nDataSize);
		if (nTrackSize == 0)
		{
			break;
		}
		lpcsData += nTrackSize;
		nDataSize -= nTrackSize;
		nEnableTracks++;
	} while (nEnableTracks < m_nMaxTracks);

	m_nEnableTracks = nEnableTracks;

	m_nStepTick = 0;
	m_nRemainTick = 0;
	m_nTotalTick = 0;
	return nEnableTracks;
}

/**
 * ステップ
 * @param[in] nStep ステップ数
 * @return 次のステップ数
 */
unsigned int CMidiFile::ExecuteStep(unsigned int nStep)
{
	do
	{
		const unsigned int nForward = (std::min)(nStep, m_nRemainTick);
		nStep -= nForward;
		m_nRemainTick -= nForward;
		if (m_nRemainTick == 0)
		{
			const unsigned int nNextStep = ExecuteStepInner(m_nStepTick);
			m_nTotalTick += m_nStepTick;
			m_nStepTick = nNextStep;
			m_nRemainTick = nNextStep;
			if (nNextStep == 0)
			{
				break;
			}
		}
	} while (nStep > 0);

	return m_nRemainTick;
}

/**
 * ステップ
 * @param[in] nStep ステップ数
 * @return 次のステップ数
 */
unsigned int CMidiFile::ExecuteStepInner(unsigned int nStep)
{
	CMidiTrack* p = m_track;
	CMidiTrack* q = p;
	CMidiTrack* r = p + m_nEnableTracks;
	unsigned int nNextStep = UINT_MAX;
	while (p < r)
	{
		const unsigned int nTrackStep = p->ExecuteStep(*this, nStep);
		if (nTrackStep)
		{
			nNextStep = (std::min)(nNextStep, nTrackStep);
			if (q != p)
			{
				*q = *p;
			}
			q++;
		}
		else
		{
			m_nEnableTracks--;
			// DEBUGLOG(TEXT("mid: enable %d"), m_nEnableTracks);
			if (m_nEnableTracks == 0)
			{
				return 0;
			}
		}
		p++;
	}
	return nNextStep;
}

/**
 * メッセージ
 * @param[in] track トラック ポインタ
 * @param[in] nMsg メッセージ タイプ
 * @param[in] pData メッセージ データ
 * @param[in] nSize メッセージ サイズ
 * @retval true 成功
 * @retval false 失敗
 */
bool CMidiFile::OnMessage(CMidiTrack* track, int nMsg, const unsigned char* pData, unsigned int nSize)
{
	if (nMsg == 0x21)
	{
		if (nSize >= 1)
		{
			track->SetModule(pData[0]);
		}
	}

	return true;
}
