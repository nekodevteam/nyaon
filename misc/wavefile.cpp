﻿/**
 * @file	wavefile.cpp
 * @brief	WAVE ファイル クラスの動作の定義を行います
 */

#include "stdafx.h"
#include "wavefile.h"
#include <algorithm>

#ifndef MAKEFOURCC
//! Four-character code macro
#define MAKEFOURCC(c0, c1, c2, c3) \
	((static_cast<unsigned int>(static_cast<unsigned char>((c0))) << 0) | \
	 (static_cast<unsigned int>(static_cast<unsigned char>((c1))) << 8) | \
	 (static_cast<unsigned int>(static_cast<unsigned char>((c2))) << 16) | \
	 (static_cast<unsigned int>(static_cast<unsigned char>((c3))) << 24))
#endif

#ifndef WAVE_FORMAT_PCM
#define WAVE_FORMAT_PCM		1			/*!< Microsoft PCM format */
#endif

/**
 * @brief RIFF ヘッダー構造体
 */
struct RiffHeader
{
	unsigned int sig;					/*!< 識別子(RIFF) */
	unsigned int size;					/*!< サイズ */
	unsigned int type;					/*!< フォーム サイズ */
};

/**
 * @brief チャンク ヘッダー構造体
 */
struct ChunkHeader
{
	unsigned int id;					/*!< チャンク識別子 */
	unsigned int size;					/*!< データ サイズ */
};

/**
 * コンストラクタ
 */
CWaveFile::CWaveFile()
	: m_fp(NULL)
	, m_bModified(false)
	, m_bReadOnly(false)
	, m_nDataStartPos(0)
	, m_nDataCurrentPos(0)
	, m_nDataSize(0)
{
}

/**
 * デストラクタ
 */
CWaveFile::~CWaveFile()
{
	Close();
}

/**
 * WAVE ファイル オープン
 * @param[in] lpFilename ファイル名
 * @param[in] bReadOnly リードオンリーフラグ
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Open(const char* lpFilename, bool bReadOnly)
{
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = NULL;
	fopen_s(&fp, lpFilename, (bReadOnly) ? "rb" : "rb+");
#else	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = fopen(lpFilename, (bReadOnly) ? "rb" : "rb+");
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

	const bool r = Open(fp, bReadOnly);
	if (!r)
	{
		Close();
	}

	return r;
}

#ifdef _WIN32
/**
 * WAVE ファイル オープン
 * @param[in] lpFilename ファイル名
 * @param[in] bReadOnly リードオンリーフラグ
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Open(const wchar_t* lpFilename, bool bReadOnly)
{
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = NULL;
	_wfopen_s(&fp, lpFilename, (bReadOnly) ? L"rb" : L"rb+");
#else	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = ::_wfopen(lpFilename, (bReadOnly) ? L"rb" : L"rb+");
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

	const bool r = Open(fp, bReadOnly);
	if (!r)
	{
		Close();
	}

	return r;
}
#endif	// _WIN32

/**
 * WAVE ファイル オープン
 * @param[in] fp ファイル
 * @param[in] bReadOnly リードオンリーフラグ
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Open(FILE* fp, bool bReadOnly)
{
	Close();

	m_bReadOnly = bReadOnly;

	bool r = false;
	do
	{
		// ファイルをオープン
		m_fp = fp;
		if (m_fp == NULL)
		{
			break;
		}

		// RIFF ヘッダ読み込み
		RiffHeader riff;
		if (::fread(&riff, sizeof(riff), 1, m_fp) != 1)
		{
			break;
		}
		if ((riff.sig != MAKEFOURCC('R', 'I', 'F', 'F')) || (riff.type != MAKEFOURCC('W', 'A', 'V', 'E')))
		{
			break;
		}

		// データの最後までチャンク読み込みを行う
		while (1 /*CONSTCOND*/)
		{
			ChunkHeader chunk;
			if (::fread(&chunk, sizeof(chunk), 1, m_fp) != 1)
			{
				break;
			}

			if (chunk.id == MAKEFOURCC('f', 'm', 't', ' '))
			{
				// チャンクデータサイズを確認
				if (chunk.size < offsetof(WAVEFORMATEX, cbSize))
				{
					break;
				}

				// チャンクデータ読み込み
				m_format.resize((std::max<unsigned int>)(chunk.size, sizeof(WAVEFORMATEX)));
				if (::fread(&m_format[0], chunk.size, 1, m_fp) != 1)
				{
					break;
				}
			}
			else if (chunk.id == MAKEFOURCC('d', 'a', 't', 'a'))
			{
				m_nDataStartPos = ::ftell(m_fp);
				m_nDataCurrentPos = 0;
				m_nDataSize = chunk.size;
				r = true;
				break;
			}
		}

		// @TODO フッタデータの読み込み

	} while (0 /*CONSTCOND*/);

	return r;
}

/**
 * WAVE ファイル作成
 * @param[in] lpFilename ファイル名
 * @param[in] nSamplesPerSec サンプリング レート
 * @param[in] nBitsPerSample サンプル ビット数
 * @param[in] nChannels チャネル数
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Create(const char* lpFilename, unsigned int nSamplesPerSec, unsigned int nBitsPerSample, unsigned int nChannels)
{
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = NULL;
	fopen_s(&fp, lpFilename, "wb");
#else	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = fopen(lpFilename, "wb");
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

	const bool r = Create(fp, nSamplesPerSec, nBitsPerSample, nChannels);
	if (!r)
	{
		Close();
		remove(lpFilename);
	}
	return r;
}

#ifdef _WIN32

/**
 * WAVE ファイル作成
 * @param[in] lpFilename ファイル名
 * @param[in] nSamplesPerSec サンプリング レート
 * @param[in] nBitsPerSample サンプル ビット数
 * @param[in] nChannels チャネル数
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Create(const wchar_t* lpFilename, unsigned int nSamplesPerSec, unsigned int nBitsPerSample, unsigned int nChannels)
{
#if (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = NULL;
	_wfopen_s(&fp, lpFilename, L"wb");
#else	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)
	FILE* fp = ::_wfopen(lpFilename, L"wb");
#endif	// (_MSC_VER >= 1400 /*Visual Studio 2005*/)

	const bool r = Create(fp, nSamplesPerSec, nBitsPerSample, nChannels);
	if (!r)
	{
		Close();
		_wremove(lpFilename);
	}
	return r;
}
#endif	// _WIN32

/**
 * WAVE ファイル作成
 * @param[in] fp ファイル
 * @param[in] nSamplesPerSec サンプリング レート
 * @param[in] nBitsPerSample サンプル ビット数
 * @param[in] nChannels チャネル数
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Create(FILE* fp, unsigned int nSamplesPerSec, unsigned int nBitsPerSample, unsigned int nChannels)
{
	Close();

	bool r = false;

	do
	{
		// ファイルを作成
		m_fp = fp;
		if (m_fp == NULL)
		{
			break;
		}

		// フォーマット作成
		const unsigned int nBlockAlign = ((nBitsPerSample * nChannels) + 7) >> 3;

		m_format.resize(sizeof(WAVEFORMATEX));
		WAVEFORMATEX* pwfx = reinterpret_cast<WAVEFORMATEX*>(&m_format[0]);
		pwfx->wFormatTag = WAVE_FORMAT_PCM;
		pwfx->nChannels = nChannels;
		pwfx->nSamplesPerSec = nSamplesPerSec;
		pwfx->nAvgBytesPerSec = nBlockAlign * nSamplesPerSec;
		pwfx->nBlockAlign = nBlockAlign;
		pwfx->wBitsPerSample = static_cast<unsigned short>(nBitsPerSample);

		// RIFF ヘッダ書き込み
		RiffHeader riff;
		riff.sig = MAKEFOURCC('R', 'I', 'F', 'F');
		riff.size = 0;
		riff.type = MAKEFOURCC('W', 'A', 'V', 'E');
		if (::fwrite(&riff, sizeof(riff), 1, m_fp) != 1)
		{
			break;
		}

		// フォーマットチャンク書き込み
		ChunkHeader fmtchunk;
		fmtchunk.id = MAKEFOURCC('f', 'm', 't', ' ');
		fmtchunk.size = offsetof(WAVEFORMATEX, cbSize);
		if (::fwrite(&fmtchunk, sizeof(fmtchunk), 1, m_fp) != 1)
		{
			break;
		}
		if (::fwrite(pwfx, 1, fmtchunk.size, m_fp) != fmtchunk.size)
		{
			break;
		}

		// データチャンク書き込み
		ChunkHeader datachunk;
		datachunk.id = MAKEFOURCC('d', 'a', 't', 'a');
		datachunk.size = 0;
		if (::fwrite(&datachunk, sizeof(datachunk), 1, m_fp) != 1)
		{
			break;
		}

		m_nDataStartPos = ::ftell(m_fp);
		m_nDataCurrentPos = 0;
		m_nDataSize = 0;

		m_bModified = true;
		r = Flush();

	} while (0 /*CONSTCOND*/);

	return r;
}

/**
 * WAVE データ読み込み
 * @param[in] lpBuffer バッファ
 * @param[in] cbBuffer バッファ サイズ
 * @return 読み込んだサイズ
 */
size_t CWaveFile::Read(void* lpBuffer, size_t cbBuffer)
{
	if (lpBuffer == NULL)
	{
		return 0;
	}

	if (m_fp == NULL)
	{
		return 0;
	}

	size_t nReadSize = m_nDataSize - m_nDataCurrentPos;
	nReadSize = (std::min)(nReadSize, cbBuffer);
	if (nReadSize <= 0)
	{
		return 0;
	}

	nReadSize = ::fread(lpBuffer, 1, nReadSize, m_fp);
	if (nReadSize <= 0)
	{
		return 0;
	}

	m_nDataCurrentPos += nReadSize;
	return nReadSize;
}

/**
 * WAVE データ書き込み
 * @param[in] lpBuffer バッファ
 * @param[in] cbBuffer バッファ サイズ
 * @return 書き込んだサイズ
 */
size_t CWaveFile::Write(const void* lpBuffer, size_t cbBuffer)
{
	if ((lpBuffer == NULL) || (cbBuffer <= 0))
	{
		return 0;
	}

	if ((m_fp == NULL) || (m_bReadOnly))
	{
		return 0;
	}

	const size_t nWrittenSize = ::fwrite(lpBuffer, 1, cbBuffer, m_fp);
	if (nWrittenSize <= 0)
	{
		return 0;
	}

	m_nDataCurrentPos += nWrittenSize;
	m_nDataSize = (std::max)(m_nDataSize, m_nDataCurrentPos);

	m_bModified = true;
	return nWrittenSize;
}

/**
 * シーク
 * @param[in] pos 位置
 * @return 位置
 */
size_t CWaveFile::Seek(size_t pos)
{
	if (m_fp == NULL)
	{
		return 0;
	}

	pos = (std::max<size_t>)(pos, 0);
	pos = (std::min)(pos, m_nDataSize);
	::fseek(m_fp, static_cast<long>(m_nDataStartPos + pos), SEEK_SET);
	return pos;
}

/**
 * クローズ
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Close()
{
	if (m_fp == NULL)
	{
		return false;
	}

	Flush();

	::fclose(m_fp);

	m_fp = NULL;
	m_bModified = false;
	m_bReadOnly = false;
	m_nDataStartPos = 0;
	m_nDataCurrentPos = 0;
	m_nDataSize = 0;
	m_format.clear();
	m_lastChunk.clear();

	return true;
}

/**
 * WAVE フォーマットを得る
 * @return WAVE フォーマット構造体のポインタ
 */
const WAVEFORMATEX* CWaveFile::GetFormat() const
{
	if (m_format.size() == 0)
	{
		return NULL;
	}
	return reinterpret_cast<const WAVEFORMATEX*>(&m_format[0]);
}

/**
 * サンプル数を得る
 * @return サンプル数
 */
unsigned int CWaveFile::GetSamples() const
{
	const WAVEFORMATEX* fmt = GetFormat();
	if (fmt == NULL)
	{
		return 0;
	}

	if (fmt->nAvgBytesPerSec == 0)
	{
		return 0;
	}

//	return MulDiv(m_nDataSize, fmt->nSamplesPerSec, fmt->nAvgBytesPerSec);
	return static_cast<unsigned int>(m_nDataSize * fmt->nSamplesPerSec / fmt->nAvgBytesPerSec);
}

/**
 * 秒数を得る
 * @return サンプル数
 */
unsigned int CWaveFile::GetSeconds() const
{
	const WAVEFORMATEX* fmt = GetFormat();
	if (fmt == NULL)
	{
		return 0;
	}

	if (fmt->nAvgBytesPerSec == 0)
	{
		return 0;
	}

	return static_cast<unsigned int>(m_nDataSize / fmt->nAvgBytesPerSec);
}

/**
 * 秒数からデータ位置を得る
 * @param[in] nSeconds 秒
 * @return データ位置
 */
size_t CWaveFile::GetDataPos(int nSeconds) const
{
	const WAVEFORMATEX* fmt = GetFormat();
	if (fmt == NULL)
	{
		return 0;
	}
	return nSeconds * fmt->nAvgBytesPerSec;
}

/**
 * フラッシュする(チャンクを更新する)
 * @retval true 成功
 * @retval false 失敗
 */
bool CWaveFile::Flush()
{
	if (m_fp == NULL)
	{
		return false;
	}
	if (!m_bModified)
	{
		return true;
	}

	bool r = false;
	const size_t nFilePos = ::ftell(m_fp);

	do
	{
		// データの最終位置
		const size_t nDataEndPos = m_nDataStartPos + m_nDataSize;

		// RIFF ヘッダを更新
		const unsigned int filesize = static_cast<unsigned int>(nDataEndPos + m_lastChunk.size());
		::fseek(m_fp, offsetof(RiffHeader, size), SEEK_SET);
		if (::fwrite(&filesize, sizeof(filesize), 1, m_fp) != 1)
		{
			break;
		}

		// DATA チャンク更新
		const unsigned int datasize = static_cast<unsigned int>(m_nDataSize);
		::fseek(m_fp, static_cast<long>(m_nDataStartPos - sizeof(ChunkHeader) + offsetof(ChunkHeader, size)), SEEK_SET);
		if (::fwrite(&datasize, sizeof(datasize), 1, m_fp) != 1)
		{
			break;
		}

		// フッターチャンクがあれば書き込む
		if (m_lastChunk.size() > 0)
		{
			::fseek(m_fp, static_cast<long>(nDataEndPos), SEEK_SET);
			if (::fwrite(&m_lastChunk[0], m_lastChunk.size(), 1, m_fp) != 1)
			{
				break;
			}
		}

		r = true;
		m_bModified = false;

	} while (0 /*CONSTCOND*/);

	::fflush(m_fp);
	::fseek(m_fp, static_cast<long>(nFilePos), SEEK_SET);
	return r;
}
