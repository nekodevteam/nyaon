﻿/**
 * @file	wavefile.h
 * @brief	WAVE ファイル クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include <vector>

#ifndef _WAVEFORMATEX_
#define _WAVEFORMATEX_
/**
 * @brief WAVEFORMATEX 構造体
 */
struct tWAVEFORMATEX
{
	unsigned short wFormatTag;			/*!< Format type */
	unsigned short nChannels;			/*!< Number of channels (i.e. mono, stereo, etc.) */
	unsigned int nSamplesPerSec;		/*!< Sample rate */
	unsigned int nAvgBytesPerSec;		/*!< For buffer estimation */
	unsigned short nBlockAlign;			/*!< Block size of data */
	unsigned short wBitsPerSample;		/*!< Number of bits per sample. */
	unsigned short cbSize;				/*!< Size, in bytes, of extra format information */
};
typedef struct tWAVEFORMATEX WAVEFORMATEX;		/*!< WAVEFORMATEX */
typedef struct tWAVEFORMATEX *PWAVEFORMATEX;	/*!< WAVEFORMATEX pointer */
typedef struct tWAVEFORMATEX *NPWAVEFORMATEX;	/*!< WAVEFORMATEX near pointer */
typedef struct tWAVEFORMATEX *LPWAVEFORMATEX;	/*!< WAVEFORMATEX far pointer */
#endif	/* _WAVEFORMATEX_ */

/**
 * @brief WAVE ファイル クラス
 */
class CWaveFile
{
public:
	CWaveFile();
	~CWaveFile();
	bool Open(const char* lpFilename, bool bReadOnly = true);
	bool Create(const char* lpFilename, unsigned int nSamplesPerSec, unsigned int nBitsPerSample, unsigned int nChannels);
#ifdef _WIN32
	bool Open(const wchar_t* lpFilename, bool bReadOnly = true);
	bool Create(const wchar_t* lpFilename, unsigned int nSamplesPerSec, unsigned int nBitsPerSample, unsigned int nChannels);
#endif	// _WIN32
	size_t Read(void* lpBuffer, size_t cbBuffer);
	size_t Write(const void* lpBuffer, size_t cbBuffer);
	size_t Seek(size_t pos);
	bool Close();
	bool Flush();
	bool IsOpened() const;
	const WAVEFORMATEX* GetFormat() const;
	size_t GetDataSize() const;
	unsigned int GetSamples() const;
	unsigned int GetSeconds() const;
	size_t GetDataPos(int nSeconds) const;

protected:
	bool Open(FILE* fp, bool bReadOnly);
	bool Create(FILE* fp, unsigned int nSamplesPerSec, unsigned int nBitsPerSample, unsigned int nChannels);

private:
	FILE* m_fp;						/*!< ファイル ハンドル */
	bool m_bModified;				/*!< 変更あり? */
	bool m_bReadOnly;				/*!< 読み出し専用? */
	size_t m_nDataStartPos;			/*!< データ開始位置 */
	size_t m_nDataCurrentPos;		/*!< データ位置 */
	size_t m_nDataSize;				/*!< データ サイズ */
	std::vector<char> m_format;		/*!< フォーマット チャンク データ */
	std::vector<char> m_lastChunk;	/*!< 最終チャンクデータ */
};

/**
 * オープン中か?
 * @retval true オープン中である
 * @retval false オープン中でない
 */
inline bool CWaveFile::IsOpened() const
{
	return (m_fp != NULL);
}

/**
 * データサイズを得る
 * @return データサイズ
 */
inline size_t CWaveFile::GetDataSize() const
{
	return m_nDataSize;
}
