﻿/**
 * @file	midiplay.h
 * @brief	MIDI ファイル再生クラスの宣言およびインターフェイスの定義をします
 */

#pragma once

#include "../misc/midifile/miditick.h"
#include "../misc/vsthost/vstmidievent.h"

/**
 * @brief MIDI ファイル再生クラス
 */
class CMidiPlay : public CMidiTick
{
public:
	CMidiPlay();
	virtual ~CMidiPlay();
#ifdef _WIN32
	bool Convert(LPCTSTR lpVst, LPCTSTR lpMidiFile, LPCTSTR lpWaveFile);
#else	// _WIN32
	bool Convert(const char* lpVst, const char* lpMidiFile, const char* lpWaveFile);
#endif	// _WIN32

protected:
	virtual void OnShortMsg(CMidiTrack* track, unsigned int nMsg);
	virtual void OnLongMsg(CMidiTrack* track, const void* pData, unsigned int nSize);

private:
	unsigned int m_nStep;		/*!< ステップ数 */
	CVstMidiEvent m_event;		/*!< イベント */
};
