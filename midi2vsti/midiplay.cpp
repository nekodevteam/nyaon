﻿/**
 * @file	midiplay.cpp
 * @brief	MIDI ファイル再生クラスの動作の定義を行います
 */

#include "stdafx.h"
#include "midiplay.h"
#include "../misc/binary.h"
#include "../misc/vsthost/vstbuffer.h"
#ifdef _WIN32
#include "../misc/vsthost/vsteditwnd.h"
#endif	// _WIN32
#include "../misc/vsthost/vsteffect.h"
#include "../misc/wavefile.h"

#define SAMPLING_RATE 48000		/*!< サンプリング レート */

/**
 * コンストラクタ
 */
CMidiPlay::CMidiPlay()
	: CMidiTick(SAMPLING_RATE)
	, m_nStep(0)
{
}

/**
 * デストラクタ
 */
CMidiPlay::~CMidiPlay()
{
}

/**
 * 変換
 * @param[in] lpVst VST ファイル
 * @param[in] lpMidiFile 入力ファイル
 * @param[in] lpWaveFile 出力ファイル
 * @retval true 成功
 * @retval false 失敗
 */
#ifdef _WIN32
bool CMidiPlay::Convert(LPCTSTR lpVst, LPCTSTR lpMidiFile, LPCTSTR lpWaveFile)
#else	// _WIN32
bool CMidiPlay::Convert(const char* lpVst, const char* lpMidiFile, const char* lpWaveFile)
#endif	// _WIN32
{
	const unsigned int nBlockSize = 512;

	CBinary file;
	if (!file.Read(lpMidiFile))
	{
		printf("Cloudn't open.\n");
		return false;
	}
	if (!SetData(&file.at(0), file.size()))
	{
		printf("Failed SetData.\n");
		return false;
	}

	// VSTi読み込み
	CVstEffect effect;
	if (!effect.Load(lpVst))
	{
		printf("Cloudn't attach VSTi.\n");
		return false;
	}

	// Effect をオープン
	effect.open();

	// サンプリング レートを設定
	effect.setSampleRate(SAMPLING_RATE);

	// ブロックサイズを設定
	effect.setBlockSize(nBlockSize);
	effect.resume();

#if 0
	effect.suspend();
	effect.setBlockSize(nBlockSize);
	effect.resume();
#endif	// 0

	effect.beginSetProgram();
	effect.setProgram(0);
	effect.endSetProgram();

	CWaveFile wave;
	if (!wave.Create(lpWaveFile, SAMPLING_RATE, 16, 2))
	{
		printf("Cloudn't create\n");
		return false;
	}

#ifdef _WIN32
	CVstEditWnd wnd;
	wnd.Create(&effect, TEXT("VSTi"), WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION | WS_MINIMIZEBOX);
#endif	// _WIN32

	m_nStep = 0;
	m_event.Clear();

	CVstBuffer input(2, nBlockSize);
	CVstBuffer output(2, nBlockSize);
	std::vector<short> pcm(2 * nBlockSize);

	while (IsPlaying())
	{
		Step(1);

		m_nStep++;
		if (m_nStep < nBlockSize)
		{
			continue;
		}

#ifdef _WIN32
		MSG msg;
		while (::PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
		CVstEditWnd::OnIdle();
#endif	// _WIN32

		m_nStep -= nBlockSize;

		effect.processEvents(m_event.GetEvents());
		effect.processReplacing(input.GetBuffer(), output.GetBuffer(), nBlockSize);
		output.GetShort(&pcm.at(0));
		wave.Write(&pcm.at(0), pcm.size() * sizeof(short));

		m_event.Clear();
	}

	effect.processEvents(m_event.GetEvents());
	effect.processReplacing(input.GetBuffer(), output.GetBuffer(), nBlockSize);
	output.GetShort(&pcm.at(0));
	wave.Write(&pcm.at(0), pcm.size() * sizeof(short));

#ifdef _WIN32
	wnd.Destroy();
#endif	// _WIN32

	return true;
}

/**
 * ショート メッセージ
 * @param[in] track トラック ポインタ
 * @param[in] nMsg メッセージ
 */
void CMidiPlay::OnShortMsg(CMidiTrack* track, unsigned int nMsg)
{
	m_event.ShortMessage(m_nStep, nMsg);
}

/**
 * ロング メッセージ
 * @param[in] track トラック ポインタ
 * @param[in] pData メッセージ データ
 * @param[in] nSize メッセージ サイズ
 */
void CMidiPlay::OnLongMsg(CMidiTrack* track, const void* pData, unsigned int nSize)
{
	m_event.LongMessage(m_nStep, pData, nSize);
}
