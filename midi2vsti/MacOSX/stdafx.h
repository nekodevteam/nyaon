//
//  stdafx.pch
//  Midi2VSTi
//
//  Created by Yui on 2016/01/15.
//  Copyright © 2016年 Yui. All rights reserved.
//

#ifndef stdafx_pch
#define stdafx_pch

// Include any system framework and library headers here that should be included in all compilation units.
// You will also need to set the Prefix Header build setting of one or more of your targets to reference this file.

#include <memory.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#endif /* stdafx_pch */
