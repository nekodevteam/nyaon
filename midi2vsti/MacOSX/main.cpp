//
//  main.cpp
//  Midi2VSTi
//
//  Created by Yui on 2016/01/15.
//  Copyright (c) 2016年 Yui. All rights reserved.
//

#include "stdafx.h"
#include <iostream>
#include "../midiplay.h"

int main(int argc, const char * argv[])
{
	if (argc < 2)
	{
		fprintf(stderr, "Usage: Midi2VSTi MIDIファイル [WAVファイル]\n");
		return 2;
	}

	char szWavFile[256];
	const char* lpWavFile;
	if (argc >= 3)
	{
		lpWavFile = argv[2];
	}
	else
	{
		lpWavFile = szWavFile;
		strcpy(szWavFile, argv[1]);

		char* lpFilename = strrchr(szWavFile, '/');
		if (lpFilename)
		{
			lpFilename++;
		}
		else
		{
			lpFilename = szWavFile;
		}
		char* lpExt = strrchr(lpFilename, '.');
		if (lpExt)
		{
			*lpExt = '\0';
		}
		strcat(lpFilename, ".wav");
	}

	CMidiPlay midi;
	return midi.Convert("/Library/Audio/Plug-Ins/VST/SOUND Canvas VA.vst/Contents/MacOS/SOUND Canvas VA", argv[1], lpWavFile) ? 0 : 1;
}
