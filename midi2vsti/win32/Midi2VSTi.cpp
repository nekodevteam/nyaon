/**
 * @file	Midi2VSTi.cpp
 * @brief	コンソール アプリケーションのエントリ ポイントを定義します。
 */

#include "stdafx.h"
#include <locale.h>
#include <shlwapi.h>
#include "../midiplay.h"
#include "../../misc/vsthost/vsteditwnd.h"

#pragma comment(lib, "shlwapi.lib")

/**
 * メイン
 * @param[in] argc 引数
 * @param[in] argv 引数
 * @return リザルト コード
 */
int _tmain(int argc, _TCHAR* argv[])
{
	_tsetlocale(LC_ALL, TEXT(""));

	if (argc < 2)
	{
		_ftprintf(stderr, TEXT("Usage: Midi2VSTi MIDIファイル [WAVファイル]\n"));
		return 2;
	}

	HINSTANCE hInstance = static_cast<HINSTANCE>(::GetModuleHandle(NULL));
	CVstEditWnd::Initialize(hInstance);

	TCHAR szModule[MAX_PATH];
	::ExpandEnvironmentStrings(TEXT("%ProgramFiles%\\Vstplugins\\Roland\\Sound Canvas VA\\SOUND Canvas VA.dll"), szModule, _countof(szModule));

	TCHAR szWavFile[MAX_PATH];
	LPCTSTR lpWavFile;
	if (argc >= 3)
	{
		lpWavFile = argv[2];
	}
	else
	{
		lpWavFile = szWavFile;
		_tcscpy_s(szWavFile, argv[1]);
		PathRenameExtension(szWavFile, TEXT(".wav"));
	}

	CMidiPlay midi;
	return midi.Convert(szModule, argv[1], lpWavFile) ? 0 : 1;
}

