// stdafx.h : 標準のシステム インクルード ファイルのインクルード ファイル、または
// 参照回数が多く、かつあまり変更されない、プロジェクト専用のインクルード ファイル
// を記述します。
//

#pragma once

#include "targetver.h"

#define NOMINMAX
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <windows.h>


// TODO: プログラムに必要な追加ヘッダーをここで参照してください。
typedef unsigned char		uint8_t;
typedef unsigned short		uint16_t;
typedef unsigned int		uint32_t;
typedef signed __int64		int64_t;
typedef unsigned __int64	uint64_t;
